import { createAction } from '@reduxjs/toolkit';
import { bookingService } from '../services/';
import { message } from 'antd';

export const SetName = createAction('BOOKING/SET_NAME');
export const SetMeetingRooms = createAction('BOOKING/SET_MEETING_ROOMS');

export const LoadRooms = (selectedDate) => async (dispatch) => {
  try {
    const roomData = await bookingService.getMeetingRooms();

    for (const room of roomData) {
      const schedule = await bookingService.getSchedule(room.id, selectedDate);
      room.schedule = {};
      schedule.map((a) => {
        room.schedule[a.startTime.toDate()] = {
          startTime: a.startTime.toDate(),
          endTime: a.endTime.toDate(),
          name: a.name,
          roomId: a.roomId
        };
      });
    }
    dispatch(SetMeetingRooms(roomData));
  } catch (e) {
    console.log(e);
  }
};

export const SaveInformation = (schedules, roomId, name, selectedDate) => async (dispatch) => {
  try {
    if(schedules.length > 0) {
      const result = await bookingService.saveBookingInformation(schedules,roomId,name);
      dispatch(LoadRooms(selectedDate));
      if(result) {
        message.success('You are all set!');
      }
    } else {
      message.error('Please select atleast one timeslot');
    }
    
  } catch (e) {
    message.error('There is a schedule conflict. Please check your timeslots again.');
    console.log(e);
  }
};
