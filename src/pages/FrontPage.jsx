import React, { useState } from 'react';
import styled from '@emotion/styled';
import Wrapper from '../components/Wrapper';
import Footer from '../components/Footer';
import { useHistory } from 'react-router-dom';
import { Button } from 'antd';

const FrontPage = () => {
  const history = useHistory();

  const LayoutStyled = styled('div')`
    position: relative;
    left: 35px;
    display: grid;
    grid-template-columns: 100wh;
    height: 300px;
  `;

  const HelloHeader = styled('h1')`
    position: relative;
    top: 20px;
    font-size: 120px;
  `;

  const WelcomeMessage = styled('h1')`
    position: relative;
    top: 0px;
    font-size: 40px;
    width: 350px;
  `;

  const ButtonStyled = styled(Button)`
    width: 400px;
    height: 70px;
    // background-color: #FF7245;
    position: relative;
    margin: 0 auto;
  `;

  return (
    <Wrapper>
      <LayoutStyled>
        <HelloHeader>Hello! 👋</HelloHeader>
        <WelcomeMessage>Hope you are having a Fabulous Day.</WelcomeMessage>
      </LayoutStyled>
      <Footer>
        <ButtonStyled
          ghost
          onClick={() => {
            history.push('/upcoming');
          }}
          type="primary"
        >
          Upcoming Schedules
        </ButtonStyled>
        <ButtonStyled
          onClick={() => {
            history.push('/name-prompt');
          }}
          type="primary"
        >
          Book a room
        </ButtonStyled>
      </Footer>
    </Wrapper>
  );
};

export default FrontPage;
