import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';
import Wrapper from '../components/Wrapper';
import Footer from '../components/Footer';
import { Select, DatePicker } from 'antd';
import ButtonStyled from '../components/ButtonStyled';
import ScheduleSelector from 'react-schedule-selector';
import { useSelector, useDispatch } from 'react-redux';
import { LoadRooms, SaveInformation } from '../actions/Booking';
import moment from 'moment';

const TimeSchedulePage = (props) => {
  const { Option } = Select;

  const LayoutStyled = styled('div')`
    position: relative;
    height: 900px;
  `;

  const Message = styled('h1')`
    position: relative;
    margin: 0 auto;
    top: 30px;
    font-size: 40px;
    width: 350px;
    text-align: center;
  `;

  const StyledPicker = styled('div')`
    position: relative;
    margin: 0 auto;
    top: 100px;
    width: 350px;
  `;

  const DropDownContainers = styled('div')`
    position: relative;
    width: 400px;
    top: 70px;
    display: grid;
    margin: 0 auto;
    grid-template-columns: 200px 200px;
    column-gap: 10px;
    grid-template-rows: auto;
    row-gap: 10px;
  `;

  const DateCell = styled('div')`
    width: 100%;
    height: 30px;
    background-color: ${(props) =>
      props.isReserved ? 'gray' : props.isSelected ? '#F7D035' : '#53A4FD'};
    text-align: center;
  `;

  const rooms = useSelector((state) => state.bookingDetail.listOfRooms);
  const name = useSelector((state) => state.bookingDetail.booking.name);
  const [schedule, setSchedule] = useState([]);
  const [selectedRoom, setSelectedRoom] = useState(0);
  const [selectedDate, setSelectedDate] = useState(moment());

  const roomSelectOnChange = (value) => {
    setSelectedRoom(value);
  };

  const selectedDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleScheduleChange = (newSchedule) => {
    if (schedule.join() != newSchedule.join()) {
      newSchedule = newSchedule.filter((a) =>
        rooms[selectedRoom]?.schedule[a] ? false : true
      );
      setSchedule(newSchedule);
    }
  };

  const dispatch = useDispatch();

  const saveChanges = async () => {
    await dispatch(
      SaveInformation(schedule, rooms[selectedRoom].id, name, selectedDate)
    );
    setSchedule([]);
  };

  useEffect(() => {
    if (selectedDate) {
      dispatch(LoadRooms(selectedDate));
    }
  }, [selectedDate]);

  return (
    <Wrapper>
      <LayoutStyled>
        <Message>
          {props.isBookingEnabled
            ? 'Lets pick a room and a time slot.'
            : 'Upcoming Schedules'}
        </Message>
        <DropDownContainers>
          <DatePicker
            defaultValue={selectedDate}
            onChange={selectedDateChange}
          />
          <Select defaultValue={selectedRoom} onChange={roomSelectOnChange}>
            {rooms.map((a, i) => {
              return (
                <Option key={i} value={i}>
                  {a.roomName}
                </Option>
              );
            })}
          </Select>
        </DropDownContainers>
        <StyledPicker>
          <ScheduleSelector
            selection={schedule}
            numDays={1}
            minTime={8}
            maxTime={22}
            startDate={selectedDate}
            hourlyChunks={1}
            renderDateLabel={(date) => {
              return <p></p>;
            }}
            renderDateCell={(datetime, selected, refSetter) => {
              return (
                <DateCell
                  isReserved={
                    rooms[selectedRoom]?.schedule[datetime] ? true : false
                  }
                  isSelected={selected}
                >
                  {rooms[selectedRoom]?.schedule[datetime]
                    ? rooms[selectedRoom]?.schedule[datetime].name
                    : ''}
                </DateCell>
              );
            }}
            onChange={(s) => handleScheduleChange(s)}
          />
        </StyledPicker>
      </LayoutStyled>
      {props.isBookingEnabled && (
        <Footer>
          <ButtonStyled
            disabled={schedule.length < 1}
            onClick={() => saveChanges()}
            type="primary"
          >
            Reserve Room
          </ButtonStyled>
        </Footer>
      )}
    </Wrapper>
  );
};

export default TimeSchedulePage;
