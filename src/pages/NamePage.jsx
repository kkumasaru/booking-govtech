import React, { useState } from 'react';
import styled from '@emotion/styled';
import Wrapper from '../components/Wrapper';
import Footer from '../components/Footer';
import { useHistory } from 'react-router-dom';
import { Button, Input } from 'antd';
import ButtonStyled from '../components/ButtonStyled';
import { useSelector, useDispatch } from 'react-redux';
import { SetName } from '../actions/Booking';

const NamePage = () => {
  const history = useHistory();

  const LayoutStyled = styled('div')`
    position: relative;
    left: 35px;
    display: grid;
    grid-template-columns: 100wh;
    height: 300px;
  `;

  const InputStyled = styled(Input)`
    position: relative;
    top: 160px;
    height: 50px;
    right: 30px;
    margin: 0 auto;
    width: 400px;
  `;

  const Message = styled('h1')`
    position: relative;
    margin: 0 auto;
    top: 200px;
    right: 35px;
    font-size: 40px;
    text-align: center;
    width: 350px;
  `;

  const name = useSelector((state) => state.bookingDetail.booking.name);
  const dispatch = useDispatch();

  return (
    <Wrapper>
      <LayoutStyled>
        <Message>What's your name?</Message>
        <InputStyled
          value={name}
          onChange={(e) => {
            dispatch(SetName(e.target.value));
          }}
          placeholder="Eg: Adam"
          autoFocus
        />
      </LayoutStyled>
      <Footer>
        <ButtonStyled
          disabled={name.trim() == ""}
          onClick={() => {
            history.push('/schedule-prompt');
          }}
          type="primary"
        >
          Continue
        </ButtonStyled>
      </Footer>
    </Wrapper>
  );
};

export default NamePage;
