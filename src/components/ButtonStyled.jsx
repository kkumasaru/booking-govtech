import styled from '@emotion/styled';
import { Button } from 'antd';

const ButtonStyled = styled(Button)`
  width: 400px;
  height: 70px;
  position: relative;
  margin: 0 auto;
  border-radius: 8px;
`;

export default ButtonStyled;
