import React from 'react';
import styled from '@emotion/styled';

const mq = (n) => {
  const breakpoints = {
    galaxyFold: 280,
    iphone5: 320,
    iphoneX: 375,
    sm: 500,
    ipad: 768,
    lg: 1200,
  };

  return `@media (min-width: ${breakpoints[n]}px)`;
};

const Container = styled('div')`
  ${mq('lg')} {
    height: auto;
    overflow: visible;
    min-height: 1000px;
    max-width: 600px;
    margin: 0 auto;
    background-color: #FFFAF4;
  }
`;

const Wrapper = ({children }) => {
  return (
    <div>
      <Container>{children}</Container>
    </div>
  );
};

export default Wrapper;
