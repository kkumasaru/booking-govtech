import React, { forwardRef } from 'react';
import styled from '@emotion/styled';

const FooterStyle = styled('div')`
  grid-area: footer;
  align-self: end;
  display: grid;
  box-sizing: border-box;
  width: 100%;
  max-width: 600px;
  position: fixed;
  z-index: 20;
  bottom: 40px;
  row-gap: 10px;
  }
`;

const Footer = (props, ref) => {
  return (
    <FooterStyle ref={ref}>
      {props.children}
    </FooterStyle>
  );
};

export default forwardRef(Footer);
