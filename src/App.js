import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect, Switch } from 'react-router-dom';
import FrontPage from './pages/FrontPage';
import NamePage from './pages/NamePage';
import TimeSchedulePage from './pages/TimeSchedulePage';

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={FrontPage} />
      <Route exact path="/name-prompt" component={NamePage} />
      <Route
        exact
        path="/schedule-prompt"
        render={(props) => <TimeSchedulePage {...props} isBookingEnabled={true} />}
      />
      <Route
        exact
        path="/upcoming"
        render={(props) => <TimeSchedulePage {...props} isBookingEnabled={false} />}
      />
      <Route component={Error} />
    </Switch>
  );
};

export default App;
