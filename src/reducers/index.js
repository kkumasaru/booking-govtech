import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import bookingDetail from './BookingDetail';

const createRootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    bookingDetail
  });
export default createRootReducer;
