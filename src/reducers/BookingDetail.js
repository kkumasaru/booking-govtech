import { createReducer } from '@reduxjs/toolkit';
import { SetMeetingRooms, SetName } from '../actions/Booking';

const initialState = {
  listOfRooms: [],
  selectedDate : Date(),
  booking: {
    name: '',
  },
};

export default createReducer(initialState, {
  [SetName](state, action) {
    state.booking.name = action.payload;
  },
  [SetMeetingRooms](state, action) {
    state.listOfRooms = action.payload;
  }
});
