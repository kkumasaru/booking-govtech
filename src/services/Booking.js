import firebase from 'firebase';
import moment from 'moment';

var firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCK,
  messagingSenderId: process.env.REACT_APP_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT,
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

const getMeetingRooms = async () => {
  const response = db.collection('meetingRooms');
  return await (await response.get()).docs.map((item) => item.data());
};

const getSchedule = async (roomId, date) => {
  const response = db
    .collection('bookings')
    .where('startTime', '>', date.startOf('day').toDate())
    .where('startTime', '<', date.endOf('day').toDate())
    .where('roomId', '==', roomId);
  return await (await response.get()).docs.map((item) => item.data());
};

const saveBookingInformation = async (schedules, roomId, name) => {
  const batch = db.batch();

  for (const schedule of schedules) {
    const documentRef = db
      .collection('bookings')
      .doc(
        `${moment(schedule).format('YYYY-MM-DD')}-${moment(schedule).format(
          'HH:MM'
        )}-${roomId}`
      );
    batch.set(documentRef, {
      startTime: schedule,
      endTime: moment(schedule).add(1, 'hour').toDate(),
      name,
      roomId,
    });
  }
  await batch.commit();
  return true;
};

export const bookingService = {
  getMeetingRooms,
  getSchedule,
  saveBookingInformation,
};
