import { bookingService } from './services/';
import moment from 'moment';

test('Loading meeting rooms', async () => {
  const result = await bookingService.getMeetingRooms();
  expect(result).not.toBeNull();;
});

test('Loading schedules', async () => {
  const result = await bookingService.getSchedule("1", moment("2021-01-11"));
  expect(result).not.toBeNull();;
});

test('Checking for schedule properties', async () => {
  const result = await bookingService.getSchedule("1", moment("2021-01-11"));
  expect(result).toEqual(          
    expect.arrayContaining([      
      expect.objectContaining({
        name: expect.any(String),
        startTime: expect.any(Object),
        roomId: expect.any(String),
        endTime: expect.any(Object),
      })
    ])
  )
});

test('Checking for meeting room object properties', async () => {
  const result = await bookingService.getMeetingRooms();
  expect(result).toEqual(          
    expect.arrayContaining([      
      expect.objectContaining({
        roomName: expect.any(String),
        id: expect.any(String)
      })
    ])
  )
});