const createHistory = require('history').createBrowserHistory; // eslint-disable-line

export default createHistory({
  forceRefresh: true
});
