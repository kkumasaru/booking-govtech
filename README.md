# Booking Meeting Rooms

It's a simple react app to make room reservations. ☺️

# Assumptions

  - for a very small team
  - Meetings on average takes about 1 hour

### Tech

The project is built using react framework incorporating redux for state management. backend is powered by rules enabled firebase database.

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ cd booking-govtech
$ yarn install
$ yarn start
```

Running the test cases

```sh
$ cd booking-govtech
$ yarn run test
```
