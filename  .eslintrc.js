module.exports = {
    extends: ['react-app', 'airbnb', 'prettier', 'prettier/react'],
    plugins: ['prettier'],
    parser: 'babel-eslint',
    env: {
      node: true,
      es6: true,
      browser: true
    },
    rules: {
      'prettier/prettier': 'error',
      'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
      'implicit-arrow-linebreak': 'off',
      'comma-dangle': 'off',
      indent: 'off',
      'react/destructuring-assignment': 'off',
      'react/prop-types': 'off',
      'no-underscore-dangle': 'off',
      'react/jsx-props-no-spreading': 'off',
      'linebreak-style': 'off',
      'import/prefer-default-export': 'off',
      camelcase: 'off',
      'jsx-a11y/label-has-associated-control': [
        'error',
        {
          required: {
            some: ['nesting', 'id']
          }
        }
      ],
      'jsx-a11y/label-has-for': [
        'error',
        {
          required: {
            some: ['nesting', 'id']
          }
        }
      ]
    }
  };
